const path = require('path')
const webpack = require('webpack')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require ('copy-webpack-plugin')
const CircularDependencyPlugin = require('circular-dependency-plugin')

// Remove this line once the following warning goes away (it was meant for webpack loader authors not users):
// 'DeprecationWarning: loaderUtils.parseQuery() received a non-string value which can be problematic,
// see https://github.com/webpack/loader-utils/issues/56 parseQuery() will be replaced with getOptions()
// in the next major version of loader-utils.'
process.noDeprecation = true;

const plugins = [
  new webpack.HotModuleReplacementPlugin(),
  new HtmlWebpackPlugin({
    inject: true,
    template: 'src/index.html',
    files: {
      css: ['css'],
    },
  }),
  new CopyWebpackPlugin ([
    { from: 'css', to: 'css' },
  ]),
  new CircularDependencyPlugin({
    exclude: /a\.js|node_modules/,
    failOnError: false,
  }),
]

const main = (options) => ({
  mode: options.mode,
  entry: options.entry.concat ([path.join (process.cwd (), 'src/global.js')]),
  output: Object.assign({
    // --- output: <dir>/app.js
    path: path.resolve(process.cwd(), 'build'),
    publicPath: '/',
  }, options.output),
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                "env",
                {
                  "modules": false
                }
              ],
              "stage-0"
            ],
            plugins: [
              'operator-overload',
              [
                "transform-object-rest-spread",
                {
                  "useBuiltIns": false,
                },
              ],
              // --- necessary??
              [
                "transform-runtime",
                {
                  "polyfill": false,
                  "regenerator": true
                }
              ],
            ]
          }
        },
      },
      {
        test: /\.(jpg|png|gif)$/,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
          },
        ],
      },
    ],
  },
  plugins: options.plugins.concat([
    new webpack.ProvidePlugin({
      fetch: 'exports-loader?self.fetch!whatwg-fetch',
    }),

    // --- UglifyJS will automatically drop any unreachable code.
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
    }),
    new webpack.NamedModulesPlugin(),
  ]),
  resolve: {
    modules: ['app', 'node_modules'],
    extensions: [
      '.js',
    ],
    mainFields: [
      'browser',
      'jsnext:main',
      'main',
    ],
  },
  devtool: options.devtool,
  // --- make web variables accessible to webpack, e.g. window
  target: 'web',
  performance: options.performance || {},
})

const devOptions = {
  mode: 'development',
  entry: [
    'eventsource-polyfill',
    'webpack-hot-middleware/client?reload=true',
  ],

  output: {
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
  },

  plugins,

  devtool: 'eval-source-map',
  performance: {
    hints: false,
  },
}

module.exports = main (devOptions)
